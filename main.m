clc;
clear all;
close all;

% ref: https://www.cnblogs.com/kkyyhh96/p/6505584.html
% main.m
img = imread('Image1_withnoise.jpg');
A = rgb2gray(img);

B = mid_filter(A, 3);

figure(1)
    subplot(1,2,1);
        imshow(A);
        title('origin');
    subplot(1,2,2);
        imshow(B);
        title('mid-value processed');

figure,imshow(roberts(A));
figure,imshow(roberts(B));

model=fspecial('sobel');
Isobel=filter2(model,A);
figure,imshow(Isobel);
Isobel=filter2(model,B);
figure,imshow(Isobel);

model=fspecial('laplacian');
Ilaplacian=filter2(model,A);
figure,imshow(Ilaplacian);
Ilaplacian=filter2(model,B);
figure,imshow(Ilaplacian);

detect_line(B)